/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Order.h
 * Author: Dylan
 *
 * Created on May 23, 2018, 9:57 PM
 */

#ifndef ORDER_H
#define ORDER_H
#include <string>

class Order {
public:
    //constructors
    Order(std::string, char, std::string, char, int, std::string, double);
    Order();
    
    //getters
    std::string getTimeStamp();
    char getMessageType();
    std::string getOrderID();
    char getBuyOrSell();
    int getShares();
    std::string getStockSymbol();
    double getPrice();
    
    //setters
    void setTimeStamp(std::string);
    void setMessageType(char);
    void setOrderID(std::string);
    void setBuyOrSell(char);
    void setShares(int);
    void setStockSymbol(std::string);
    void setPrice(double);
    
private:
    std::string timeStamp;
    char messageType;
    std::string orderID;
    char BuyOrSell;
    int shares;
    std::string stockSymbol;
    double price;
    
};

#endif /* ORDER_H */

