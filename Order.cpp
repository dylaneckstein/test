/*  Author: Dylan
 * Created on May 23, 2018, 9:57 PM
 */

#include "Order.h"
#include <string>

using namespace std;

    //constructors
    Order::Order(string tStamp, char mType, string oID, char BoS, int shares, string sSymbol, double price){
        this->timeStamp = tStamp;
        this->messageType = mType;
        this->orderID = oID;
        this->BuyOrSell = BoS;
        this->shares = shares;
        this->stockSymbol = sSymbol;
        this->price = price;
    }

    Order::Order(){
    }


    //getters
    string Order::getTimeStamp() {return this->timeStamp;}  
    char Order::getMessageType() {return this->messageType;}
    string Order::getOrderID() {return this->orderID;}
    char Order::getBuyOrSell() {return this->BuyOrSell;}
    int Order::getShares() {return this->shares;}
    string Order::getStockSymbol() {return this->stockSymbol;}
    double Order::getPrice() {return this->price;}


    //setters
    void Order::setTimeStamp(string timeStamp) {this->timeStamp = timeStamp;}
    void Order::setMessageType(char messagetype) {this->messageType = messageType;}
    void Order::setOrderID(string orderID) {this->orderID = orderID;}
    void Order::setBuyOrSell(char BuyOrSell) {this->BuyOrSell = BuyOrSell;}
    void Order::setShares(int shares) {this->shares = shares;}
    void Order::setStockSymbol(string stockSymbol) {this->stockSymbol = stockSymbol;}
    void Order::setPrice(double price) {this->price = price;}
        