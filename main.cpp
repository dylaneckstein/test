/* Author: Dylan Eckstein
 * Created on May 23, 2018, 3:59 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <sstream>
#include "Order.h"
#include <list>
#include <unordered_map>


using namespace std;

/*
 * 
 */


Order temp = Order();  //temporary order
//this map holds all the orders. The key is the orderID and the value is the Order Object.
std::unordered_map<std::string, Order> oMap = {std::make_pair("0", temp)};
//this map holds all the orders executed. The key is the Stock Symbol and the value is the amount of shares traded.
std::unordered_map<std::string, int> exeMap;    //this holds all the trades executed

void AddOrder(string line);     //'A'
void ExeOrder(string line);     //'E'
void Trade(string line);        //'P'
void CancelOrder(string line);  //'X'

void printMap();    //not needed but good tool for debugging
void topTen();      //creates the output asked for

int main() {
    oMap.erase("0");    //since I am using a custom class I had to initialize the map with a temporary Order and then erase it
    oMap.rehash(100000);    //I cheated on the sizes just because I know the input, they should be plenty large enough
    exeMap.rehash(500);
    
    //open file
    ifstream pitchIn;
    pitchIn.open("out.txt");
    string line;
    
    //parse one line at a time
    if (pitchIn.is_open()){
        while ( !pitchIn.eof() ){
            getline(pitchIn,line);  //line = the whole message as a string
            int n = line.length();
            if (n > 0){     //check to make sure line is not null
                switch(line[9]){
                    case 'A':
                        AddOrder(line);
                        break;
                    case 'E':
                        ExeOrder(line);
                        break;
                    case 'P':
                        Trade(line);
                        break;
                    case 'X':
                        CancelOrder(line);
                        break;
                }   //end switch
            }
            
        }   //end reading
        pitchIn.close();
    }
    else{   //if file not found
        cout << "Cannot find \"out.txt\"" << endl;
    }
    //print output
    topTen();
    system("pause");
    
    return 0;
}

/* Most common message. Parses the message, creates an Order instance, and adds it to our order map.
 * Adds the stock symbol to the executed orders map if the symbol isn't in the map yet.
 * input: message as a string       output: none
 */
void AddOrder(string line){
    //this block is parsing the message into the variables
    line = line.substr(1, line.length());    //take off the 'S'
    string timeStamp = line.substr(0,8);
    char messageType = line[8];
    string orderID = line.substr(9,12);
    char BuyOrSell = line[21];
    int shares = stoi(line.substr(22,6));
    string stockSymbol = line.substr(28,6);
    double price = stod(line.substr(34,10));
    
    //create instance of Order and add it to our map
    Order order = Order(timeStamp, messageType, orderID, BuyOrSell, shares, stockSymbol, price);
    oMap.insert(make_pair(order.getOrderID(), order));
    
    //if stock isn't in our stock map then add it
    if(exeMap.find(order.getOrderID()) == exeMap.end()){
        exeMap.insert(make_pair(order.getStockSymbol(), 0));
    }
}


/* Adds the shares executed to the executed orders map, then subtracts the amount of shares from the order.
 * Erases order from map if the transaction completes the order.
 * input: message as a string       output: none
 */
void ExeOrder(string line){
    line = line.substr(1, line.length());    //take off the 'S'
    string orderID = line.substr(9,12);
    int exeShares = stoi(line.substr(21,6));
    
    Order order = oMap.at(orderID);
    exeMap.at(order.getStockSymbol()) += exeShares;

    order.setShares(order.getShares() - exeShares);
    if(order.getShares() == 0){
        oMap.erase(orderID);
    }
}


/* Adds the shares executed to the executed orders map.
 * input: message as a string       output: none
 */
void Trade(string line){
    line = line.substr(1, line.length());    //take off the 'S'
    int exeShares = stoi(line.substr(22,6));
    string stockSymbol = line.substr(28,6);
    exeMap[stockSymbol] += exeShares;
}


/* Subtracts shares from an order and erases it if necessary.
 * input: message as a string       output: none
 */
void CancelOrder(string line){
    line = line.substr(1, line.length());    //take off the 'S'
    string orderID = line.substr(9,12);
    int numShares = stoi(line.substr(21,6));
    Order order = oMap.at(orderID);
    order.setShares(order.getShares() - numShares);
    if(order.getShares() == 0){
        oMap.erase(orderID);
    }
}


/* Gives the output requested
 * input: none     output: The top 10 traded stocks along with their volume
 */
void topTen(){
    string topT[10];
    int topAmts[10];
    for(int i = 0; i < 10; i++){
        int highestAmt = 0;
        string mostTraded;
        for ( auto &p : exeMap ){
            if(p.second > highestAmt){
                mostTraded = p.first;
                highestAmt = p.second;
            }
        }
        topT[i] = mostTraded;
        topAmts[i] = highestAmt;
        exeMap.erase(mostTraded);
    }
    for (int i = 0; i < 10; i++){
        string symb = topT[i];
        int difference = 6 - symb.length();
        for (int i = 0; i < difference; i++){
            symb += " ";
        }
        string amt = to_string(topAmts[i]);
        difference = 5 - amt.length();
        string buffer = "";
        for (int i = 0; i < difference; i++){
            buffer += " ";
        }
        buffer += amt;
        
        symb += buffer;
        cout << symb << endl;
    }
}

void printMap(){
    for ( auto &p : oMap ){
            cout << endl << p.first << " => " << p.second.getStockSymbol() << " => " << p.second.getShares();
        }
    
//    for ( auto &p : exeMap ){
//        if(p.second > 0){
//            cout << endl << p.first << " => " << p.second;
//        }
//    }
}